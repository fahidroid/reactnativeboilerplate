import React from 'react';

import {mainStack} from 'src/config/navigator';

import {createStackNavigator} from '@react-navigation/stack';

import HomeTabs from './home-tabs';


import HelpScreen from 'src/screens/profile/help';
import ContactScreen from 'src/screens/profile/contact';
import AccountScreen from 'src/screens/profile/account';
import ChangePasswordScreen from 'src/screens/profile/change-password';


import EditAccount from 'src/screens/profile/edit-account';

import Page from 'src/screens/profile/Page';

const Stack = createStackNavigator();

function MainStack() {
  return (
    <Stack.Navigator
      initialRouteName={mainStack.home_tab}
      screenOptions={{gestureEnabled: false}}>
      <Stack.Screen
        options={{headerShown: false}}
        name={mainStack.home_tab}
        component={HomeTabs}
      />
     
      <Stack.Screen
        options={{headerShown: false}}
        name={mainStack.contact}
        component={ContactScreen}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={mainStack.help}
        component={HelpScreen}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={mainStack.account}
        component={AccountScreen}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name={mainStack.change_password}
        component={ChangePasswordScreen}
      />
 
     
      <Stack.Screen
        options={{headerShown: false}}
        name={mainStack.edit_account}
        component={EditAccount}
      />
      
      <Stack.Screen
        options={{headerShown: false}}
        name={mainStack.page}
        component={Page}
      />
    </Stack.Navigator>
  );
}

export default MainStack;
