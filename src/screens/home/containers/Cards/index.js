import React from 'react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {withTranslation} from 'react-i18next';

import {StyleSheet, Dimensions, View} from 'react-native';
import Container from 'src/containers/Container';
import Heading from 'src/containers/Heading';

import {ThemedView, Header, Text} from 'src/components';

import {languageSelector} from 'src/modules/common/selectors';

import {borderRadius} from 'src/components/config/spacing';

import action from 'src/utils/action';
import {margin} from '../../../../components/config/spacing';

const {width} = Dimensions.get('window');

const initHeader = {
  style: {},
};

class Cards extends React.Component {
  render() {
    return (
      <View>
        <View style={styles.cardContainer}>
          <View style={styles.mainTextContainer}>
            <Text h2 bold>
              {' '}
              {this.props.mainText}
            </Text>
            <Text h4 colorThird>
              {' '}
              {this.props.mainHeader}
            </Text>
          </View>
          <View style={styles.infoTextContainer}>
            <View alignItems="center">
              <Text h6 colorThird color={'#FDAA83'}>
                {this.props.infoHeader}
              </Text>
              <Text h3 bold style={{color: '#F99563'}}>
                {this.props.infoValue}
              </Text>
            </View>
            <View alignItems="center" margin={margin.base}>
              <Text h6 colorThird color={'#FDAA83'}>
                {this.props.infoHeader2}
              </Text>
              <Text h3 bold>
                {this.props.infoValue2}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    paddingTop: 0,
  },
  cardContainer: {
    borderWidth: 3,

    borderRightColor: '#eee',
    borderTopColor: '#eee',
    borderBottomColor: '#eee',

    borderTopRightRadius: borderRadius.big,
    borderBottomRightRadius: borderRadius.big,
    margin: margin.large,
    height: 150,
    borderLeftWidth: 10,
    borderLeftColor: '#FE864C',
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  mainTextContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoTextContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = (state) => ({
  language: languageSelector(state),
});

Cards.defaultProps = {
  widthComponent: width,
};

export default compose(withTranslation(), connect(mapStateToProps))(Cards);
