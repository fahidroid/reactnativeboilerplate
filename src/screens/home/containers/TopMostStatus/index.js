import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';

import { StyleSheet, Dimensions, View } from 'react-native';
import Container from 'src/containers/Container';
import Heading from 'src/containers/Heading';
import countriesAr from '../../../../locales/countries-ar.json'
import { ThemedView, Header, Text, Image } from 'src/components';

import { languageSelector } from 'src/modules/common/selectors';

import { borderRadius } from 'src/components/config/spacing';

import action from 'src/utils/action';
import { margin } from '../../../../components/config/spacing';
import { flagUrl } from '../../../../config/images';
function toLocaleStrings(n = 0) {
  return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}
const { width } = Dimensions.get('window');

const initHeader = {
  style: {},
};

class TopMostStatus extends React.Component {
  render() {
    const { data } = this.props;
    if (data.length) {
      return (
        <View style={styles.container}>
          <Text h4 bold>
            {this.props.heading}
          </Text>
          <View style={styles.line} />
          {data.map((co) => {
            return (
              <View>
                <View style={styles.row1}>
                  <Text h2>
                    {co.countryCode
                      .toUpperCase()
                      .replace(/./g, (char) =>
                        String.fromCodePoint(char.charCodeAt(0) + 127397),
                      )}{' '}
                    {this.props.language == 'ar'
                      ? countriesAr[co.countryCode.toLowerCase()]
                      : co.countryName}
                  </Text>
                  <Text h5 style={{ color: '#F23D38' }}>
                    🤧 +{toLocaleStrings(co.dailyConfirmed)}
                  </Text>
                </View>
                <View style={styles.row1}>
                  <Text h5>
                    🤧 {toLocaleStrings(co.totalConfirmed)} {' | '}
                    💪🏻 {toLocaleStrings(co.totalRecovered)} {' | '}
                    😭 {toLocaleStrings(co.totalDeaths)}{' '}
                  </Text>
                  <Text h5 style={{ color: '#F23D38' }}>
                    😭 +{toLocaleStrings(co.dailyDeaths)}
                  </Text>
                </View>
                <View style={styles.line} />
              </View>
            );
          })}
        </View>
      );
    } else {
      return <Text>loading</Text>;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    margin: margin.large,
  },
  line: {
    marginVertical: margin.small,
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  row1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  flag: {
    height: 25,
    width: 25,
  },
  cardContainer: {
    borderWidth: 3,

    borderRightColor: '#eee',
    borderTopColor: '#eee',
    borderBottomColor: '#eee',

    borderTopRightRadius: borderRadius.big,
    borderBottomRightRadius: borderRadius.big,
    margin: margin.large,
    height: 150,
    borderLeftWidth: 10,
    borderLeftColor: '#FE864C',
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  mainTextContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoTextContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = (state) => ({
  language: languageSelector(state),
});

TopMostStatus.defaultProps = {
  widthComponent: width,
};

export default compose(
  withTranslation(),
  connect(mapStateToProps),
)(TopMostStatus);
