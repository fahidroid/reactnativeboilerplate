import React from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import HomeDrawer from './home-drawer';
import Me from 'src/screens/profile/me';


import Tabbar from 'src/containers/Tabbar';

import SettingScreen from 'src/screens/profile/setting';
import {homeTabs} from 'src/config/navigator';

const Tab = createBottomTabNavigator();

export default function HomeTabs() {
  return (
    <Tab.Navigator tabBar={(props) => <Tabbar {...props} />}>
      <Tab.Screen name={homeTabs.home_drawer} component={HomeDrawer} />
      <Tab.Screen name={homeTabs.me} component={Me} />
      <Tab.Screen name={homeTabs.settings} component={SettingScreen} />

      
    </Tab.Navigator>
  );
}
