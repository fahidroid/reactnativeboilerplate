//  wordpress host server
export const API = 'https://app.medicaqatar.com';

export const CONSUMER_KEY = 'ck_32a3ec8e79436972114dcf7d793bc8df40b004a6';
export const CONSUMER_SECRET = 'cs_66cfed4203c28e234455f09358a17a83680abec1';

export const CORONA_API =
  'https://api.coronatracker.com/v3/stats/worldometer/global';
export const ALL_COUNTRY_CORONA_API =
  'https://api.coronatracker.com/v3/stats/worldometer/country';
export const EVERY_DAY_COVID_API =
  'https://api.coronatracker.com/v3/stats/worldometer/totalTrendingCases';
export default {
  API_ENDPOINT: API,
  CORONA_API,
  CONSUMER_KEY,
  CONSUMER_SECRET,
  EVERY_DAY_COVID_API,
};
