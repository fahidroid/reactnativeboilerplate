import React from 'react';

import {connect} from 'react-redux';
import moment from 'moment';
import {DrawerActions} from '@react-navigation/native';

import {ScrollView, View, Dimensions, ActivityIndicator} from 'react-native';

import {ThemedView, Header, Text} from 'src/components';
import {IconHeader, Logo, CartIcon} from 'src/containers/HeaderComponent';
import ModalHomePopup from 'src/containers/ModalHomePopup';
import LottieView from 'lottie-react-native';

import {fetchSettingSuccess} from 'src/modules/common/actions';
import {
  dataConfigSelector,
  toggleSidebarSelector,
  expireConfigSelector,
} from 'src/modules/common/selectors';

import {fetchSetting} from 'src/modules/common/service';

// Containers
import Slideshow from './home/containers/Slideshow';
import TypeOfCaseSlider from './home/containers/TypeOfCaseSlider';
import {
  fetchGlobalCovidStatus,
  changeCovidPage,
} from '../modules/common/actions';
import {
  allCountryCovidStatusSelector,
  coivdPageLoadingSelector,
  coivdPageSelector,
  dailyCovidDatasSelector,
  globalCovidStatusSelector,
  languageSelector,
  top3ConfirmedCountriesSelector,
  top3DeathsCountriesSelector,
  top3RecoveredCountriesSelector,
} from '../modules/common/selectors';
import {fromJS} from 'immutable';
import {withTranslation} from 'react-i18next';
import {compose} from 'recompose';
import Cards from './home/containers/Cards';
import StatusTable from './home/containers/TopMostStatus';
import TopMostStatus from './home/containers/TopMostStatus';
import {BarChart, Grid, XAxis, LineChart, YAxis} from 'react-native-svg-charts';
import {margin} from '../components/config/spacing';

const {width} = Dimensions.get('window');
function toLocaleStrings(n = 0) {
  return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}
const containers = {
  slideshow: Slideshow,
  // categories: CategoryList,
  // products: ProductList,
  // productcategory: ProductCategory,
  // banners: Banners,
  // text: TextInfo,
  // countdown: CountDown,
  // blogs: BlogList,
  // testimonials: Testimonials,
  // button: Button,
  // vendors: Vendors,
  // search: Search,
  // divider: Divider,
};

const widthComponent = (spacing) => {
  if (!spacing) {
    return width;
  }
  const marginLeft =
    spacing.marginLeft && parseInt(spacing.marginLeft, 10)
      ? parseInt(spacing.marginLeft, 10)
      : 0;
  const marginRight =
    spacing.marginRight && parseInt(spacing.marginRight, 10)
      ? parseInt(spacing.marginRight, 10)
      : 0;
  const paddingLeft =
    spacing.paddingLeft && parseInt(spacing.paddingLeft, 10)
      ? parseInt(spacing.paddingLeft, 10)
      : 0;
  const paddingRight =
    spacing.paddingRight && parseInt(spacing.paddingRight, 10)
      ? parseInt(spacing.paddingRight, 10)
      : 0;
  return width - marginLeft - marginRight - paddingLeft - paddingRight;
};

const fillss = 'rgb(134, 65, 244)';
const data2 = [50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80];
const datass = [
  50,
  10,
  40,
  95,
  -4,
  -24,
  null,
  85,
  undefined,
  0,
  35,
  53,
  -53,
  24,
  50,
  -20,
  -80,
];
class HomeScreen extends React.Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch(fetchGlobalCovidStatus());
    // this.getConfig();
    // dispatch(fetchCategories());
  }

  renderContainer(config) {
    const Container = containers[config.type];
    if (!Container) {
      return null;
    }
    return (
      <View key={config.id} style={config.spacing && config.spacing}>
        <Container
          {...config}
          widthComponent={widthComponent(config.spacing)}
        />
      </View>
    );
  }

  renderPageDetails = () => {
    const {
      globalCovidStatus,
      covidPage,
      t,
      top3ConfirmedCountries,
      allCountryCovidStatus,
      top3DeathsCountries,
      top3RecoveredCountries,
      dailyCovidDatas,
    } = this.props;

    let totalConfirmed = toLocaleStrings(
      globalCovidStatus?.data?.totalConfirmed,
    );
    let totalDeaths = toLocaleStrings(globalCovidStatus?.data.totalDeaths);
    let totalRecovered = toLocaleStrings(
      globalCovidStatus?.data.totalRecovered,
    );
    let totalNewDeaths = toLocaleStrings(
      globalCovidStatus?.data.totalNewDeaths,
    );
    let totalNewCases = toLocaleStrings(globalCovidStatus?.data.totalNewCases);
    let totalCasesPerMillionPop = toLocaleStrings(
      globalCovidStatus?.data.totalCasesPerMillionPop,
    );
    let fatalityrate =
      (globalCovidStatus?.data.totalDeaths /
        globalCovidStatus?.data.totalConfirmed) *
      100;
    let recoveryrate =
      (globalCovidStatus?.data.totalRecovered /
        globalCovidStatus?.data.totalConfirmed) *
      100;
    switch (covidPage) {
      case 0:
        return (
          <View style={{flex: 1}}>
            <Cards
              mainText={totalDeaths}
              mainHeader={t('common:text_deaths')}
              infoHeader={t('common:text_newcases')}
              infoValue={`+${totalNewDeaths}`}
              infoHeader2={t('common:text_fatalityrate')}
              infoValue2={`${fatalityrate.toFixed(2)}%`}
            />
            <TopMostStatus
              data={top3DeathsCountries}
              heading={t('common:text_top3deathheading')}
            />
          </View>
        );
      case 1:
        return (
          <View>
            <Cards
              mainText={totalConfirmed}
              mainHeader={t('common:text_confirmed')}
              infoHeader={t('common:text_newcases')}
              infoValue={`+${totalNewCases}`}
              infoHeader2={t('common:text_total_confirmed_per_million')}
              infoValue2={totalCasesPerMillionPop}
            />
            <TopMostStatus
              data={top3ConfirmedCountries}
              heading={t('common:text_top3confirmedheading')}
            />
          </View>
        );
      case 2:
        return (
          <View>
            <Cards
              mainText={totalRecovered}
              mainHeader={t('common:text_recovered')}
              infoHeader={t('common:text_newcases')}
              infoValue={'----'}
              infoHeader2={t('common:text_recoveryrate')}
              infoValue2={`${recoveryrate.toFixed(2)}%`}
            />
            <TopMostStatus
              data={top3RecoveredCountries}
              heading={t('common:text_top3recoveredheading')}
            />
          </View>
        );
    }
  };

  render() {
    const {
      loading,
    } = this.props;

    return (
      <ThemedView isFullView>
        {loading == true ? (
          <LottieView
            source={require('../assets/lottie/loading.json')}
            autoPlay
            loop
          />
        ) : (
          <ScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}>
            <TypeOfCaseSlider />
            {this.renderPageDetails()}
          </ScrollView>
        )}

        <ModalHomePopup />
      </ThemedView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    config: dataConfigSelector(state),
    toggleSidebar: toggleSidebarSelector(state),
    language: languageSelector(state),
    expireConfig: expireConfigSelector(state),
    
    covidPage: coivdPageSelector(state),
    globalCovidStatus: globalCovidStatusSelector(state).toJS(),
    allCountryCovidStatus: allCountryCovidStatusSelector(state),
    top3ConfirmedCountries: top3ConfirmedCountriesSelector(state),
    top3DeathsCountries: top3DeathsCountriesSelector(state),
    top3RecoveredCountries: top3RecoveredCountriesSelector(state),
    dailyCovidDatas: dailyCovidDatasSelector(state),
    loading: coivdPageLoadingSelector(state),
  };
};

export default compose(connect(mapStateToProps), withTranslation())(HomeScreen);
