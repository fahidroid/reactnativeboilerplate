import React, {Component} from 'react';

import {connect} from 'react-redux';

import {
  StyleSheet,
  View,
  Dimensions,
  ImageBackground,
  Platform,
  TouchableOpacity,
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import {Text, Avatar, Image, withTheme, Icon} from 'src/components';
import Container from 'src/containers/Container';
import Pagination from 'src/containers/Pagination';

import {languageSelector} from 'src/modules/common/selectors';

import {padding} from 'src/components/config/spacing';
import {compose} from 'recompose';
import {withNavigation} from '@react-navigation/compat';
import {withTranslation} from 'react-i18next';
import moment from 'moment/min/moment-with-locales';
import {changeCovidPage} from '../../../../modules/common/actions';

const {width} = Dimensions.get('window');

class TypeOfCaseSlider extends Component {
  state = {
    pagination: 0,
  };

  render() {
    const {
      theme,
      language,
      fields,
      widthComponent,
      clickGoPage,
      t,
      dispatch,
    } = this.props;
    const {pagination} = this.state;
    const strDateInWords = (date) => {
      if (!date) {
        return null;
      }
      return moment(date).format('dddd, DD MMM');
    };

    const widthImage = width;
    const heightImage = 390;
    const images = [1, 2, 3];
    const data = [
      t('common:text_worldwide_death_cases'),
      t('common:text_worldwide_confirmed_cases'),
      t('common:text_worldwide_recovered_cases'),
    ];

    const widthView = widthComponent - 2;
    const heightView = (widthView * heightImage) / widthImage;
    const heightFooter = 25;
    const heightScroll = 80;

    const autoplayDelay = 1000;
    const autoplayInterval = 1800;

    const styleImage = {
      width: widthView,
      height: heightView,
      justifyContent: 'flex-end',
    };

    return (
      <Container disable={'all'}>
        <Carousel
          ref={(c) => {
            this._carousel = c;
          }}
          data={data}
          renderItem={({item, index}) => (
            <View
              style={{height: heightScroll, marginTop: padding.large}}
              // key={i} // we will use i for the key because no two (or more) elements in an array will have the same index
            >
              {/*... we will return a square Image with the corresponding object as the source*/}
              <View style={styles.headerContainer}>
                <TouchableOpacity
                  onPress={() => {
                    switch (pagination) {
                      case 0:
                        this._carousel.snapToItem(data.length - 1, true);
                        break;
                      default:
                        this._carousel.snapToItem(pagination - 1, true);
                    }
                  }}>
                  <Icon name="chevron-left" size={26} isRotateRTL />
                </TouchableOpacity>
                <View style={styles.viewInfo}>
                  <Text secondary>{strDateInWords(Date.now())}</Text>
                  <Text h4 bold>
                    {item}
                  </Text>

                  <Text
                    light
                    style={[
                      styles.textTitle,
                      item.subTitle && item.subTitle.style,
                    ]}>
                    {item.subTitle &&
                      item.subTitle.text &&
                      `${item.subTitle.text[language]} `}
                    {item.title && (
                      <Text medium style={[styles.textTitle, item.title.style]}>
                        {item.title.text && item.title.text[language]}
                      </Text>
                    )}
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={() => {
                    switch (pagination) {
                      case data.length - 1:
                        this._carousel.snapToItem(0, true);
                        break;
                      default:
                        this._carousel.snapToItem(pagination + 1, true);
                    }
                  }}>
                  <Icon name="chevron-right" size={26} isRotateRTL />
                </TouchableOpacity>
              </View>
            </View>
          )}
          sliderWidth={widthView}
          itemWidth={widthView}
          inactiveSlideOpacity={1}
          inactiveSlideScale={1}
          autoplayDelay={autoplayDelay}
          autoplayInterval={autoplayInterval}
          loop
          autoplay={false}
          onSnapToItem={(index) => {
            this.setState({pagination: index});
            dispatch(changeCovidPage(index));
          }}
        />
        {true && (
          <Pagination
            activeVisit={pagination}
            count={images.length}
            containerStyle={styles.viewPagination}
          />
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  viewImage: {
    resizeMode: 'stretch',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewInfo: {
    // paddingHorizontal: padding.base,
    marginTop: padding.large,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textTitle: {
    fontSize: 40,
  },
  viewPagination: {
    // position: 'absolute',
    // bottom: 0,
    // left: padding.base,
    justifyContent: 'center',
    // marginRight: 50 + padding.large,
  },
  buttonNext: {
    position: 'absolute',
    bottom: 0,
    right: padding.large,
    ...Platform.select({
      android: {
        elevation: 4,
      },
    }),
  },
});

const mapStateToProps = (state) => ({
  language: languageSelector(state),
});

TypeOfCaseSlider.defaultProps = {
  widthComponent: width,
};

export default compose(
  connect(mapStateToProps),
  withNavigation,
  withTheme,
  withTranslation(),
)(TypeOfCaseSlider);

// export default connect(mapStateToProps)(withTheme(TypeOfCaseSlider));
