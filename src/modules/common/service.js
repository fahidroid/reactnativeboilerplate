import axios from 'axios';
import request from 'src/utils/fetch';
import {PLUGIN_NAME} from 'src/config/development';
import {
  CORONA_API,
  ALL_COUNTRY_CORONA_API,
  EVERY_DAY_COVID_API,
} from '../../config/api';

export const fetchSetting = () => request.get(`/${PLUGIN_NAME}/v1/settings`);
export const fetchConfig = () => request.get(`/${PLUGIN_NAME}/v1/configs`);
export const fetchTemplate = () =>
  request.get(`/${PLUGIN_NAME}/v1/template-mobile`);
export const fetchCountries = () => request.get('/wc/v3/data/countries');
export const fetchPaymentGateways = () =>
  request.get('/wc/v3/payment_gateways');

export const fetchAllCountryCovidStatus = () =>
  axios.get(ALL_COUNTRY_CORONA_API);

export const fetchGlobalCovidStatus = () => axios.get(`${CORONA_API}`);
export const fetchDailyCovidData = () => axios.get(`${EVERY_DAY_COVID_API}`);
