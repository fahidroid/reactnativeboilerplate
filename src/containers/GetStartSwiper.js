import React from 'react';
import { withTranslation } from 'react-i18next';
import {
  StyleSheet,
  ScrollView,
  View,
  Image,
  Animated,
  Dimensions,
  I18nManager,
} from 'react-native';
import { Button, Text } from 'src/components';
import Container from './Container';
import Pagination from './Pagination';

import { margin } from 'src/components/config/spacing';
import { compose } from 'redux';
import { connect } from 'react-redux';
import {
  languageSelector,
  listLanguageSelector,
} from 'src/modules/common/selectors';
import { changeLanguage } from 'src/modules/common/actions';

import i18n from 'src/config-i18n';
import LottieView from 'lottie-react-native'
import RNRestart from 'react-native-restart';

const { width } = Dimensions.get('window');
const WIDTH_IMAGE = width - 10;
const HEIGHT_IMAGE = (WIDTH_IMAGE * 390) / 375;

class GetStartSwiper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollX: new Animated.Value(0),
      height: 0,
      loading: false,
      language: props.language,
    };
    console.log(this.props.language, this.props.languages);
  }
  reloadApp(language) {
    const isRTL = i18n.dir(language) === 'rtl';
    I18nManager.forceRTL(isRTL);
    // Reload
    if (isRTL !== I18nManager.isRTL) {
      RNRestart.Restart();
      // Updates.reloadFromCache(); // For expo
    }
  }

  toggleLanguage = (language) => {
    const { dispatch } = this.props;
    dispatch(changeLanguage(language == 'en' ? 'ar' : 'en'));
    setTimeout(() => {
      this.setState({
        loading: false,
      });
      this.reloadApp();
    }, 1000);
  };
  render() {
    const { t } = this.props;
    const { scrollX } = this.state;
    const position = Animated.divide(scrollX, width);

    const data = [
      {
        lottie: require('src/assets/lottie/get-start-1.json'),
        title: t('getting:text_title_1'),
        subtitle: t('getting:text_subtitle_1'),
      },
      {
        lottie: require('src/assets/lottie/get-start-4.json'),
        title: t('getting:text_title_2'),
        subtitle: t('getting:text_subtitle_2'),
      },
      {
        lottie: require('src/assets/lottie/get-start-3.json'),
        title: t('getting:text_title_3'),
        subtitle: t('getting:text_subtitle_3'),
      },
    ];
    return (
      <>
        <ScrollView style={styles.container}>
          <ScrollView
            horizontal={true}
            pagingEnabled={true}
            showsHorizontalScrollIndicator={false}
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: { x: scrollX },
                  },
                },
              ],
              {
                useNativeDriver: false,
              },
            )}
            scrollEventThrottle={16}>
            {data.map((swiper, index) => (
              <View key={index} style={styles.viewItem}>
                <LottieView source={swiper.lottie} style={styles.image} autoPlay loop autoSize resizeMode='stretch' />
                <Container style={styles.viewInfo}>
                  <Text h1 medium style={[styles.text, styles.title]}>
                    {swiper.title}
                  </Text>
                </Container>
              </View>
            ))}
          </ScrollView>
          <Pagination
            type="animated"
            activeVisit={position}
            count={data.length}
            containerStyle={styles.viewPagination}
          />
        </ScrollView>
        <Container style={styles.viewButton}>
          <Button
            loading={this.state.loading}
            buttonStyle={{ marginBottom: 10 }}
            type="outline"
            raised
            title={this.props.language == 'en' ? 'لعربية' : 'English'}
            onPress={() => this.toggleLanguage(this.props.language)}
          />
          <Button
            title={t('home:text_getting_start')}
            onPress={this.props.handleGettingStarted}
          />
        </Container>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    width: WIDTH_IMAGE,
    height: HEIGHT_IMAGE,
    alignContent: 'center',
    justifyContent: "center",
    alignItems: "center",

  },
  viewItem: {
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center"
  },
  viewInfo: {
    marginVertical: margin.big + margin.small,
  },
  text: {
    textAlign: 'center',
  },
  title: {
    marginBottom: margin.small,
  },
  viewPagination: {
    marginTop: margin.small,
    marginBottom: margin.big,
    justifyContent: 'center',
  },
  viewButton: {
    marginVertical: margin.big,
  },
});

const mapStateToProps = (state) => {
  return {
    language: languageSelector(state),
    languages: listLanguageSelector(state).toJS(),
  };
};

export default compose(
  withTranslation(),
  connect(mapStateToProps),
)(GetStartSwiper);
