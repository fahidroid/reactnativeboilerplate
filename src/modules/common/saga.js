import {put, call, takeEvery, delay} from 'redux-saga/effects';

import * as Actions from './constants';

import {
  fetchCountries,
  fetchPaymentGateways,
  fetchGlobalCovidStatus,
  fetchAllCountryCovidStatus,
  fetchDailyCovidData,
} from './service';
// import {fetchShippingZoneMethod} from '../cart/service';

const getTop3Deaths = (data) =>
  data.sort((a, b) =>
    a.totalDeaths < b.totalDeaths ? 1 : b.totalDeaths < a.totalDeaths ? -1 : 0,
  );
const getTop3Confirmed = (data) =>
  data.sort((a, b) =>
    a.totalConfirmed < b.totalConfirmed
      ? 1
      : b.totalConfirmed < a.totalConfirmed
      ? -1
      : 0,
  );
const getTop3Recovered = (data) =>
  data.sort((a, b) =>
    a.totalRecovered < b.totalRecovered
      ? 1
      : b.totalRecovered < a.totalRecovered
      ? -1
      : 0,
  );
/**
 * Fetch countries saga
 * @returns {IterableIterator<*>}
 */
function* fetchCountrySaga() {
  try {
    const countries = yield call(fetchCountries);
    yield put({
      type: Actions.FETCH_COUNTRY_SUCCESS,
      payload: countries,
    });
  } catch (error) {
    yield put({
      type: Actions.FETCH_COUNTRY_ERROR,
    });
  }
}

/**
 * Fetch payment gateways saga
 * @returns {IterableIterator<*>}
 */
function* fetchPaymentGatewaySaga() {
  try {
    const countries = yield call(fetchPaymentGateways);
    yield put({
      type: Actions.FETCH_PAYMENT_GATEWAYS_SUCCESS,
      payload: countries,
    });
  } catch (error) {
    yield put({
      type: Actions.FETCH_PAYMENT_GATEWAYS_ERROR,
    });
  }
}

/**
 * Fetch payment gateways saga
 * @returns {IterableIterator<*>}
 */
function* fetchGlobalCovidStatusSaga() {
  try {
    yield put({
      type: Actions.SHOW_COVID_PAGE_LOADING,
    });
    const statuses = yield call(fetchGlobalCovidStatus);
    console.log(statuses.data);
    const status2 = yield call(fetchAllCountryCovidStatus);
    const status3 = yield call(fetchDailyCovidData);

    yield put({
      type: Actions.FETCH_GLOBAL_COVID_STATUS_SUCCESS,
      payload: statuses.data,
    });
    yield put({
      type: Actions.FETCH_ALL_COUNTRY_STATUS_SUCCESS,
      payload: status2.data,
    });
    yield put({
      type: Actions.FETCH_TOP_3DEATHS_SUCCESS,
      payload: getTop3Deaths(status2.data).splice(0, 5),
    });
    yield put({
      type: Actions.FETCH_TOP_3CONFIRMED_SUCCESS,
      payload: getTop3Confirmed(status2.data).splice(0, 5),
    });
    yield put({
      type: Actions.FETCH_TOP_3RECOVERED_SUCCESS,
      payload: getTop3Recovered(status2.data).splice(0, 5),
    });
    yield put({
      type: Actions.FETCH_DAILY_COVID_DATA,
      payload: status3.data,
    });

    yield put({
      type: Actions.HIDE_COVID_PAGE_LOADING,
    });
  } catch (error) {
    yield put({
      type: Actions.FETCH_GLOBAL_COVID_STATUS_ERROR,
    });
  }
}

/**
 * Fetch shipping method not covered by zones saga
 * @returns {IterableIterator<*>}
 */
function* fetchShippingMethodNotCoveredByZonesSaga() {
  try {
    // const data = yield call(fetchShippingZoneMethod, 0);
    // yield put({
    //   type: Actions.FETCH_SHIPPING_METHOD_NOT_COVER_BY_ZONE_SUCCESS,
    //   payload: data,
    // });
  } catch (error) {
    // yield put({
    //   type: Actions.FETCH_SHIPPING_METHOD_NOT_COVER_BY_ZONE_ERROR,
    // });
  }
}

export default function* commonSaga() {
  yield takeEvery(Actions.FETCH_COUNTRY, fetchCountrySaga);
  yield takeEvery(
    Actions.FETCH_GLOBAL_COVID_STATUS,
    fetchGlobalCovidStatusSaga,
  );
  yield takeEvery(Actions.FETCH_PAYMENT_GATEWAYS, fetchPaymentGatewaySaga);
  yield takeEvery(
    Actions.FETCH_SHIPPING_METHOD_NOT_COVER_BY_ZONE,
    fetchShippingMethodNotCoveredByZonesSaga,
  );
}
